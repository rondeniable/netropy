# netropy
<b>Overview:</b> This is a small web based tool for storing and rapidly applying Netropy N91 network emulation profiles. This tool is for use when needing to run testing on applications to test their network resillance. Basically, 
configure each profile you would like to test, then from the web interface you can choose the emulation profile you would 
like to use. The tool will automatically set the emulation back to none, and then apply your new settings. 
<br>
<br>
<b>Requirements:</b><br>
1.  Apposite N91 Network Emulation Appliance (http://www.apposite-tech.com/products/netropy-N91.html)<br>
2.  Apache WebServer (RHEL/CentOS Tested)<br>
3.  MySQL (Should work on most versions, but testing on 5.0.95)<br>
<br>
<br>
<b>Included:</b> This includes the following:<br>
  Simple responsive (BootStrap) webpage that allows for the quick select of profiles<br>
  Simple JSON interface for programming multiple changes (perfect for QA testing)<br>
<br>
<b>Not Included:</b> This DOES NOT include the following:<br>
  The Apposite N91.. obviously.<br>
  There is no web based configuration, configuration is done via the config.php and database<br>
<br>
<br>
<b>How to get started:</b><br>
1. Create an Apache Virtualhost. The only thing required here is the php_value include_path. This saves on messy includes for both scriting and web deployments. An example can be found in apache.cfg<br>
<br>
2. Edit the config.php with the appropriate options to allow for connection to database and to Apposite N91<br>
   vi config.php<br>
<br>
3. Setup Database via your favorite database editor<br>
  Setup Database User<br>
  Setup Database<br>
  mysql -e 'create database appositeControl';<br>
  Import Database<br>
  mysql appositeControl < appositeControl.sql<br>
<br>
4. Adding Emulation Profiles<br>
  First, add emulation profile entry into the NETWORK_PROFILE table.<br>
  Second, add each configuration you would like applied for said profile.<br>
