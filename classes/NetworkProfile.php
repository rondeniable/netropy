<?php
/**
 * Description of NetworkProfile
 *
 * @author ron.davis
 */

include_once('classes/Apposite.php');

class NetworkProfile 
{
    
    private $logger = "";
    private $Db = "";
    
    
    public function __construct() 
    {
       
        $this->logger = &Log::singleton('file', LOGFILE, TYPE);
    
        $this->logger->debug('class.' . __CLASS__ . ' :: ' . __FUNCTION__ . ' :: Entering');
        
        /*
         * Setup Database
         */
        $database = fncGetDBConfig();
        $this->logger->debug('class.' . __CLASS__ . ' :: ' . __FUNCTION__ . ' :: Opening Connection to Database');
        try
        {
            $this->Db['SC'] = new PDO($database['SC']['dsn'], $database['SC']['username'], $database['SC']['password']);
            $this->Db['SC']->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            $this->Db['SC']->setAttribute(PDO::ATTR_EMULATE_PREPARES, false);

        }
        catch (PDOException $e)
        {
            $this->logger->error('class.' . __CLASS__ . ' :: ' . __FUNCTION__ . " :: " . $e->getMessage());
            $this->logger->error('class.' . __CLASS__ . ' :: ' . __FUNCTION__ . " :: DSN [" . $database['dsn'] . "]");
            throw new Exception('SYSTEM_ERROR');
        }

       
    }
    
    
    public function getNetworkProfiles()
    {
        
        $this->logger->debug('class.' . __CLASS__ . ' :: ' . __FUNCTION__ . ' :: Entering');
                
        try
        {
            $query = "SELECT * from NETWORK_PROFILE";
            $stmt = $this->Db['SC']->prepare($query);
            $stmt->execute();
        } 
        catch (Exception $ex) 
        {
              $logger->log($e->getMessage(), PEAR_LOG_DEBUG);  
        }
        
        $results = array();
        while($row = $stmt->fetch(PDO::FETCH_ASSOC))
        {
            $results[] = $row;
        }
        
        return $results;
        
    }
    
    public function getKeyName($key)
    {
        
        $this->logger->debug('class.' . __CLASS__ . ' :: ' . __FUNCTION__ . ' :: Entering');
                
        try
        {
            $query = "SELECT `key`, name from NETWORK_PROFILE_OPTIONS " .
                        "where `key` = :key limit 1";
            $stmt = $this->Db['SC']->prepare($query);
            $stmt->bindParam(':key', $key, PDO::PARAM_STR);
            $stmt->execute();
        } 
        catch (Exception $e) 
        {
              $this->logger->log($e->getMessage(), PEAR_LOG_DEBUG);  
        }
        
        $row = $stmt->fetch(PDO::FETCH_OBJ);
        
        return $row->name;
        
    }
    
    public function getOptions($networkProfileID)
    {
        
        $this->logger->debug('class.' . __CLASS__ . ' :: ' . __FUNCTION__ . ' :: Entering');
                
        try
        {
            $query = "SELECT DISTINCT(`key`), name from NETWORK_PROFILE_OPTIONS";
            $stmt = $this->Db['SC']->prepare($query);
            $stmt->execute();
        } 
        catch (Exception $ex) 
        {
              $logger->log($e->getMessage(), PEAR_LOG_DEBUG);  
        }
        
        $results = array();
        while($row = $stmt->fetch(PDO::FETCH_OBJ))
        {
            
            $rowKey = $row->key;
            
            try
            {
                
                $query = "SELECT np.id, npo.name, npo.`key`, " .
                            "npo.value, npo.network_profile_id, npo.display " .
                            "from NETWORK_PROFILE as np " .
                            "LEFT JOIN NETWORK_PROFILE_OPTIONS as npo on np.id = npo.network_profile_id " .
                            "where npo.network_profile_id = :network_profile_id " .
                            "AND npo.`key` = :rowKey";
                $stmt1 = $this->Db['SC']->prepare($query);
                $stmt1->bindParam(':network_profile_id', $networkProfileID, PDO::PARAM_INT);
                $stmt1->bindParam(':rowKey', $rowKey, PDO::PARAM_STR);
                $stmt1->execute();
                
            } 
            catch (Exception $e) 
            {
                $this->logger->log($e->getMessage(), PEAR_LOG_DEBUG);  
            }
            
            $row1 = $stmt1->fetch(PDO::FETCH_OBJ);
            if ($stmt1->rowCount() < 1)
            {
                $results[] = array('key' => $rowKey, 'name' => $this->getKeyName($rowKey), 'value' => 'NULL', 'display' => '0');
            }
            else 
            {
                $results[] = array('key' => $rowKey, 'name' => $row1->name, 'value' => $row1->value, 'display' => $row1->display);
            }
            
        }
        
        return $results;
        
        
        
    }
    
    public function getAllOptions()
    {
        
        $this->logger->debug('class.' . __CLASS__ . ' :: ' . __FUNCTION__ . ' :: Entering');
                
        try
        {
            $query = "SELECT DISTINCT(`key`) from NETWORK_PROFILE_OPTIONS";
            $stmt = $this->Db['SC']->prepare($query);
            $stmt->execute();
        } 
        catch (Exception $e) 
        {
              $this->logger->log($e->getMessage(), PEAR_LOG_DEBUG);  
              $this->logger->log($query);  
        }
        
        $results = array();
        while($row = $stmt->fetch(PDO::FETCH_ASSOC))
        {
            $results[] = $row;
        }
        
        return $results;
        
    }
    
    public function applyNetworkProfile($networkProfileID, $engineID, $pathID)
    {
        
        $this->logger->debug('class.' . __CLASS__ . ' :: ' . __FUNCTION__ . ' :: Entering');
        
        // Get the Network Profile
        $networkProfileOptions = $this->getOptions($networkProfileID);
        
        // Get a list of all available options for clearing
        $allOptions = $this->getAllOptions();
        
        // Call Apposite Device
        $Apposite = new Apposite();
        $Apposite->setVariable('engineID', $engineID);
        $Apposite->setVariable('pathID', $pathID);
        
        try
        {
            
            // Clear all Settings
            for ($index = 0; $index < count($allOptions); $index++) 
            {
                $Apposite->clearOption($allOptions[$index]['key']);
            }
            
            // Loop through each option and set them
            for ($index = 0; $index < count($networkProfileOptions); $index++) 
            {
                if ($networkProfileOptions[$index]['value'] != "NULL")
                {
                    $Apposite->setOption($networkProfileOptions[$index]['key'], $networkProfileOptions[$index]['value']);
                }
            }
            
        }
        catch (Exception $e)
        {
            throw new Exception ($e->getMessage());
        }
        
    }
    
}