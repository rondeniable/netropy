<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Shell
 *
 * @author ron.davis
 */
class Shell 
{
    private $host = "";
    private $username = "";
    private $password = "";
    private $cmd = "";
    private $output = "";
    private $returnStatus = "";
    
    private $logger = "";
    private $Db = "";
    
    
    public function __construct() 
    {
       
        $this->logger = &Log::singleton('file', LOGFILE, TYPE);
        $this->logger->debug('class.' . __CLASS__ . ' :: ' . __FUNCTION__ . ' :: Entering');
    
    }
    
    public function execute()
    {
        
        $this->logger->debug('class.' . __CLASS__ . ' :: ' . __FUNCTION__ . ' :: Entering');
        
        // Build Run Command
        $runCommand = SSH_PATH . " " . SSH_OPTIONS ." " . APPOSITE_USERNAME . "@" . APPOSITE_HOSTNAME . " '" . $this->cmd . "' 2>&1";
        
        // Execute Run Command
        $this->logger->debug('class.' . __CLASS__ . ' :: ' . __FUNCTION__ . ' :: Running [' . $runCommand . ']');
        
        try
        {
            exec($runCommand, $this->output, $this->returnStatus);
        }
        catch (Exception $e)
        {
            $this->logger->error('class.' . __CLASS__ . ' :: ' . __FUNCTION__ . ' :: ERROR [' . $e->getMessage() . ']');
            throw new Exception ($e->getMessage());
        }
        
        if ($this->returnStatus != "0")
        {
            $this->logger->debug('class.' . __CLASS__ . ' :: ' . __FUNCTION__ . ' :: Output [' . print_r($this->output, true) . ']');
            $this->logger->debug('class.' . __CLASS__ . ' :: ' . __FUNCTION__ . ' :: Return Status [' . $this->returnStatus . ']');
            throw new Exception('print_r($this->output, true)');
        }
        
        return $this->returnStatus;
        
    }
    
    public function getVariable($getter)
    {
        $this->logger->debug('class.' . __CLASS__ . ' :: ' . __FUNCTION__ . ' :: Entering');

        if (!$this->isLoaded)
        {
            $this->load;
        };

        return $this->$getter;
    }


   public function setVariable($setter, $value)
   {
       $this->logger->debug('class.' . __CLASS__ . ' :: ' . __FUNCTION__ . ' :: Entering');

       if (isset($this->$setter))
       {
           $this->$setter = $value;
       }
       else
       {
           throw new Exception('Unknown variable name $this->[' . $setter . ']');
       }
   }
   
}
