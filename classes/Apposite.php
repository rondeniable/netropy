<?php

/*
 * Developer: Ron Davis
 * Date: 2014-01-21
 * Name: Apposite.php
 * Purpose: This class is designed for control of the Apposite network device
 * 
 */

include_once('classes/Shell.php');

class Apposite
{
    private $shell = "";
    private $engineID = "";
    private $pathID = "";
    private $pPort = "";
    private $sPort = "";
    
    private $logger = "";
    private $Db = "";
    
    
    public function __construct() 
    {
       
        $this->logger = &Log::singleton('file', LOGFILE, TYPE);
    
        $this->logger->debug('class.' . __CLASS__ . ' :: ' . __FUNCTION__ . ' :: Entering');
        
        /*
         * Get a Shell
         */
        $this->shell = new Shell();
        $this->shell->setVariable('host', APPOSITE_HOSTNAME);
        $this->shell->setVariable('username', APPOSITE_USERNAME);
        $this->shell->setVariable('password', APPOSITE_PASSOWRD);
        
    }
    
    
    public function applyNetworkProfile()
    {
        
        $this->logger->debug('class.' . __CLASS__ . ' :: ' . __FUNCTION__ . ' :: Entering');
        
        // Set the Delay
        $cmd = 'engine ' . $this->engineID . ' path ' . $this->pathID . ' set delay constant 20 port 1 to port 2';
        $this->shell->setVariable('cmd', $cmd);
        $this->shell->execute();
        
    }
    
    public function buildCommand($key, $value, $method)
    {
        
        // Block for Set Methods
        if ($method == 'set')
        {
            
            $this->logger->debug('class.' . __CLASS__ . ' :: ' . __FUNCTION__ . ' :: Value [' . $value . ']');
            
            /*
             * Base on Key, build command
             */
            $cmd = "";
            switch ($key) 
            {
                
                /*
                 * Delay
                 */
                case 'inboundDelay':
                    $values = explode('|', $value);
                    $cmd = sprintf('engine %u path %u set delay normal %u %u reordering %s port %u to port %u', $this->engineID, $this->pathID, $values[0], $values[1], $values[2], $this->pPort, $this->sPort);
                    break;

                case 'outboundDelay':
                    $values = explode('|', $value);
                    $cmd = sprintf('engine %u path %u set delay normal %u %u reordering %s port %u to port %u', $this->engineID, $this->pathID, $values[0], $values[1], $values[2], $this->sPort, $this->pPort);
                    break;

                /*
                 * Uniform Delay
                 */
                case 'uniformDelay':
                    $values = explode('|', $value);
                    $cmd = sprintf('engine %u path %u set delay uniform %u %u reordering %s port %u to port %u', $this->engineID, $this->pathID, $values[0], $values[1], $values[2],$this->pPort, $this->sPort);
                    break;
                
                /*
                 * Background Traffic
                 */
                case 'inboundBackground':
                    $cmd = sprintf('engine %u path %u set bg random rate %f burst 1500 port %u out', $this->engineID, $this->pathID, $value, $this->pPort);
                    break;
                
                case 'outboundBackground':
                    $cmd = sprintf('engine %u path %u set bg random rate %f burst 1500 port %u out', $this->engineID, $this->pathID, $value, $this->sPort);
                    break;
                
                /*
                 * Loss
                 */
                case 'inboundLoss':
                    $cmd = sprintf('engine %u path %u set loss random %f port %u to port %u', $this->engineID, $this->pathID, $value, $this->pPort, $this->sPort);
                    break;

                case 'outboundLoss':
                    $cmd = sprintf('engine %u path %u set loss random %f port %u to port %u', $this->engineID, $this->pathID, $value, $this->sPort, $this->pPort);
                    break;
                
                case 'randomLoss':
                    $cmd = sprintf('engine %u path %u set loss random %f port %u to port %u', $this->engineID, $this->pathID, $value, $this->pPort, $this->sPort);
                    break;
                
                /*
                 * Bandwidth
                 */
                case 'primaryBandToWAN':
                    $cmd = sprintf('engine %u path %u set bw fixed %f Mbps port %u out', $this->engineID, $this->pathID, $value, $this->pPort);
                    break;
                
                case 'secondaryBandToWAN':
                    $cmd = sprintf('engine %u path %u set bw fixed %f Mbps port %u out', $this->engineID, $this->pathID, $value, $this->sPort);
                    break;
                
                default:
                    break;
                
                /*
                 * Corruption
                 */
                case 'corruption':
                    $cmd = sprintf('engine %u path %u set corruption ber %s port %u to port %u', $this->engineID, $this->pathID, $value, $this->pPort, $this->sPort);
                    break;
                
                /*
                 * Reordering
                 */
                case 'inboundReordering':
                    $cmd = sprintf('engine %u path %u set reordering random %u min 1 port %u to port %u', $this->engineID, $this->pathID, $value, $this->pPort, $this->sPort);
                    break;

                case 'outboundReordering':
                    $cmd = sprintf('engine %u path %u set reordering random %u max 10 port %u to port %u', $this->engineID, $this->pathID, $value, $this->sPort, $this->pPort);
                    break;
                
                /*
                 * Duplication
                 */
                case 'inboundDuplication':
                    $cmd = sprintf('engine %u path %u set duplication random %u port %u to port %u', $this->engineID, $this->pathID, $value, $this->pPort, $this->sPort);
                    break;

                case 'outboundDuplication':
                    $cmd = sprintf('engine %u path %u set duplication random %u port %u to port %u', $this->engineID, $this->pathID, $value, $this->sPort, $this->pPort);
                    break;

                /*
                 * Queuing
                 */
                case 'inboundQueue':
                    $cmd = sprintf('engine %u path %u set queue droptail %s port %u out', $this->engineID, $this->pathID, $value, $this->pPort);
                    break;
                
                case 'outboundQueue':
                    $cmd = sprintf('engine %u path %u set queue droptail %s port %u out', $this->engineID, $this->pathID, $value, $this->sPort);
                    break;
                
                /*
                 * MTU Limit
                 */
                case 'mtu_limit':
                    $cmd = sprintf('engine %u path %u set mtu-limit fixed %u send-icmp enabled fragmentation standard port %u out', $this->engineID, $this->pathID, $value, $this->pPort);
                    break;
                
            }
            
        }
        // Block for Clear Methods
        elseif ($method == 'clear') 
        {
            
            /*
             * Base on Key, build command
             */
            $cmd = "";
            switch ($key) 
            {
                /*
                 * Delay
                 */
                case 'inboundDelay':
                    $cmd = sprintf('engine %u path %u set delay none port %u to port %u', $this->engineID, $this->pathID, $this->pPort, $this->sPort);
                    break;

                case 'outboundDelay':
                    $cmd = sprintf('engine %u path %u set delay none port %u to port %u', $this->engineID, $this->pathID, $this->sPort, $this->pPort);
                    break;

                /*
                 * Uniform Delay
                 */
                case 'uniformDelay':
                    $values = explode('|', $value);
                    $cmd = sprintf('engine %u path %u set delay none port %u to port %u', $this->engineID, $this->pathID,$this->pPort, $this->sPort);
                    break;
                
                /*
                 * Background Traffic
                 */
                case 'inboundBackground':
                    $cmd = sprintf('engine %u path %u set bg none port %u out', $this->engineID, $this->pathID, $this->pPort);
                    break;
                
                case 'outboundBackground':
                    $cmd = sprintf('engine %u path %u set bg none port %u out', $this->engineID, $this->pathID, $this->sPort);
                    break;
                
                /*
                 * Loss
                 */
                case 'inboundLoss':
                    $cmd = sprintf('engine %u path %u set loss none port %u to port %u', $this->engineID, $this->pathID, $this->pPort, $this->sPort);
                    break;

                case 'outboundLoss':
                    $cmd = sprintf('engine %u path %u set loss none port %u to port %u', $this->engineID, $this->pathID, $this->sPort, $this->pPort);
                    break;
                
                case 'randomLoss':
                    $cmd = sprintf('engine %u path %u set loss none port %u to port %u', $this->engineID, $this->pathID, $this->pPort, $this->sPort);
                    break;
                
                /*
                 * Bandwidth
                 */
                case 'primaryBandToWAN':
                    $cmd = sprintf('engine %u path %u set bw fixed %u Mbps port %u out', $this->engineID, $this->pathID, '45', $this->pPort);
                    break;
                
                case 'secondaryBandToWAN':
                    $cmd = sprintf('engine %u path %u set bw fixed %u Mbps port %u out', $this->engineID, $this->pathID, '45', $this->sPort);
                    break;
                
                default:
                    break;
        
                /*
                 * Corruption
                 */
                case 'corruption':
                    $cmd = sprintf('engine %u path %u set corruption none port %u to port %u', $this->engineID, $this->pathID, $this->pPort, $this->sPort);
                    break;
                
                /*
                 * Reordering
                 */
                case 'inboundReordering':
                    $cmd = sprintf('engine %u path %u set reordering none port %u to port %u', $this->engineID, $this->pathID, $this->pPort, $this->sPort);
                    break;

                case 'outboundReordering':
                    $cmd = sprintf('engine %u path %u set reordering none port %u to port %u', $this->engineID, $this->pathID, $this->sPort, $this->pPort);
                    break;
                
                
                /*
                 * Duplication
                 */
                case 'inboundDuplication':
                    $cmd = sprintf('engine %u path %u set duplication none port %u to port %u', $this->engineID, $this->pathID, $this->pPort, $this->sPort);
                    break;

                case 'outboundDuplication':
                    $cmd = sprintf('engine %u path %u set duplication none port %u to port %u', $this->engineID, $this->pathID, $this->sPort, $this->pPort);
                    break;
                
                /*
                 * Queuing
                 */
                case 'inboundQueue':
                    $cmd = sprintf('engine %u path %u set queue default port %u out', $this->engineID, $this->pathID, $this->pPort);
                    break;
                
                case 'outboundQueue':
                    $cmd = sprintf('engine %u path %u set queue default port %u out', $this->engineID, $this->pathID, $this->sPort);
                    break;
                
                /*
                 * MTU Limit
                 */
                case 'mtu_limit':
                    $cmd = sprintf('engine %u path %u set mtu-limit none port %u out', $this->engineID, $this->pathID, $this->pPort);
                    break;
       
            }
            
        }
        
        // Return the built command
        return $cmd;
      
    }
    
    
    public function clearOption($key)
    {
        
        $this->logger->debug('class.' . __CLASS__ . ' :: ' . __FUNCTION__ . ' :: Entering');
        
        $this->buildPorts();
        
        // Build the Command
        $cmd = $this->buildCommand($key, '', 'clear');
        
        // Set the Delay
        $this->shell->setVariable('cmd', $cmd);
        try 
        {
            $this->shell->execute();
        } 
        catch (Exception $e) 
        {
            throw new Exception($e->getMessage());
        }
        
    }
    
    
    public function buildPorts()
    {
        
        /*
         * Based on the engineID we know the ports
         */
        switch ($this->engineID) {
            case 1:
                $this->pPort = 1;
                $this->sPort = 2;
                break;
            
            case 2:
                $this->pPort = 3;
                $this->sPort = 4;
                break;
            
            case 3:
                $this->pPort = 5;
                $this->sPort = 6;
                break;
            
            case 4:
                $this->pPort = 7;
                $this->sPort = 8;
                break;
            
        }
    }
    
    public function setOption($key, $value)
    {
        
        $this->logger->debug('class.' . __CLASS__ . ' :: ' . __FUNCTION__ . ' :: Entering');
        
        $this->buildPorts();
        
        // Build the Command
        $cmd = $this->buildCommand($key, $value, 'set');
        
        // Set the Delay
        $this->shell->setVariable('cmd', $cmd);
        try 
        {
            $this->shell->execute();
        } 
        catch (Exception $e) 
        {
            throw new Exception($e->getMessage());
        }
        
    }
    
    
    public function getVariable($getter)
    {
        $this->logger->debug('class.' . __CLASS__ . ' :: ' . __FUNCTION__ . ' :: Entering');

        if (!$this->isLoaded)
        {
            $this->load;
        };

        return $this->$getter;
    }


   public function setVariable($setter, $value)
   {
       $this->logger->debug('class.' . __CLASS__ . ' :: ' . __FUNCTION__ . ' :: Entering');

       if (isset($this->$setter))
       {
           $this->$setter = $value;
       }
       else
       {
           throw new Exception('Unknown variable name $this->[' . $setter . ']');
       }
   }
   
    
}