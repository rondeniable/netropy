<?php 

class Request
{
	private $request = "";

	function __construct() 
	{
		//print "<pre>";
		//print_r($_REQUEST);
		//print "</pre>";

		$this->request = $_REQUEST;
	}
	
	public function get($input)
	{
		if (array_key_exists($input, $_REQUEST)) 
		{
			return $_REQUEST[$input];
		}
		else
		{
			throw Exception ("Unknown Request String [" . $input . "]");
		}
	}
}

// Include the Request Object
$Request = new Request();

// Fixup for scripts vs web scripts
if (isset($_SERVER['REMOTE_ADDR']))
{
	define("REMOTE_ADDR", $_SERVER['REMOTE_ADDR']);
}  
else
{
	define("REMOTE_ADDR", '172.16.3.121');
}
?>
