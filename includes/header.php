<!DOCTYPE html>
<html>
  
  <head>
    <title>Adaptivity Control Center</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    
    <!-- CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/other.css" rel="stylesheet">
 
    <!-- JS -->
    <script src="https://code.jquery.com/jquery.js"></script>
    <script src="/js/bootstrap.min.js"></script>
    
    <script type="text/javascript" src="/js/ddslick.min.js?date=<?= date('Y-m-d:s') ?>"></script>
    
  </head>
  
  <body>