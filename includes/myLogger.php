<?php
    //
    // Logging
    //
    global $logger;
    $logger = &Log::singleton('file', LOGFILE, TYPE);

    if (isset($_SERVER['REMOTE_ADDR']))
    {
        $startMsg = 'Starting Request: ' . $_SERVER['REQUEST_URI'];
    }
    else
    {
        $startMsg = "Starting CLI";
    }

    $logger->log($startMsg, PEAR_LOG_DEBUG);
    
?>
