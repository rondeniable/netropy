<?php

require_once 'Log.php';
require_once 'includes/Request.php';
require_once 'config/config.php';
require_once 'includes/myLogger.php';
include_once('base/HTTPRequest.php');

/*
 * Setup HTTPRequest
 */
$logger->debug('page.' . basename(__FILE__) . ' :: Setup HTTPRequest');
$HTTPRequest = new HTTPRequest();