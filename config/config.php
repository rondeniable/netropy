<?php

/*
 * Logging Configuration
 */
define('LOGFILE','/var/log/php/net.log');
define('TYPE','Site');

/*
 * Apposite Device
 */
define('APPOSITE_HOSTNAME', 'fqdn');
define('APPOSITE_USERNAME', 'admin');
define('APPOSITE_PASSOWRD', '');

/*
 * SSH Options
 */
define('SSH_PATH', '/usr/bin/ssh');
define('SSH_OPTIONS', '-v -o UserKnownHostsFile=/dev/null -o StrictHostKeyChecking=no');

/*
 * Database Configuration
 */
function fncGetDBConfig()
{

    $DBConfig = array(
            'SC' => array(
                       'dsn'    => 'mysql:host=localhost;dbname=SC;charset=utf8',
                       'username'   => 'db_user',
                       'password'   => 'db_pass'
                   )
    );

    return $DBConfig;

}
