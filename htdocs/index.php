<?php 

include_once('includes/appHeader.php'); 
include_once('includes/header.php'); 
include_once('classes/NetworkProfile.php'); 

$NetworkProfile = new NetworkProfile();
$networkProfiles = $NetworkProfile->getNetworkProfiles();

$engineID = $HTTPRequest->get('engineID');
$pathID = $HTTPRequest->get('pathID');

?>

<script type="text/javascript">
    
    $( document ).ready(function() {
        var ddData = [
            <?php
                for ($index = 0; $index < count($networkProfiles); $index++) 
                {
                    ?>
                        {
                            text: "<?= $networkProfiles[$index]['name'] ?>",
                            value: <?= $networkProfiles[$index]['id'] ?>,
                            selected: <?php if ($networkProfiles[$index]['orderBy'] == 1) { echo "true,"; } else { echo "false,"; } ?>
                            description: "<?= $networkProfiles[$index]['description'] ?>",
                            imageSrc: "<?= $networkProfiles[$index]['imageSrc'] ?>"
                        }<?php if ($index < count($networkProfiles)-1) { echo ","; } ?>
                    <?php
                }
            ?>
        ];
        
        $('#myDropdown').ddslick({
            data: ddData,
            width: 275,
            imagePosition: "left",
            selectText: "Select Network Emulation",
            onSelected: function(data) {
                console.log(data);
                
                // 
                // Hide all options
                //
                var options = document.getElementsByClassName('options');
                for(var i=0; i<options.length; ++i){
                    var s = options[i].style;
                    s.display = 'none';
                };
                
                // 
                // Show only one selected
                //
                document.getElementById('tbody_' + data.selectedData.value).style.display = "table-row-group";
                
                <?php
                // Determine which engines to alter
                if (!isset($engineID))
                {
                    ?>
                        // 
                        // Make calls and Change settings to new items for engines 1 and 2
                        //
                        var url = '/json/networkProfile.php?method=applyNetworkProfile&engineID=4&pathID=4&networkProfileID= ' + data.selectedData.value;

                        $.ajax({
                            url: url
                        });

                        var url = '/json/networkProfile.php?method=applyNetworkProfile&engineID=4&pathID=5&networkProfileID= ' + data.selectedData.value;

                        $.ajax({
                            url: url
                        });
                        
                    <?php
                } 
                else
                {
                    ?>
                        // 
                        // Make calls and Change settings to new items for engines <?= $engineID ?>
                        //     
                        var url = '/json/networkProfile.php?method=applyNetworkProfile&engineID=<?= $engineID ?>&pathID=<?= $pathID ?>&networkProfileID= ' + data.selectedData.value;

                        $.ajax({
                            url: url
                        });
                        
                    <?php
                }
                ?>
            }
        });
    });
    
</script>

<div class="controlContainer">
    <div id="myDropdown"></div>
    <div class="displaySettings">
        <table class="table table-striped table-bordered table-hover table-condensed" style="width: 100%;">
            <caption>Connection Settings</caption>
            <thead>
                <td class="hKey">Key</td>
                <td class="hValue">Value</td>
            </thead>
            <?php
                // Loop through and print
                for ($index = 0; $index < count($networkProfiles); $index++) 
                {
                    // Determine visibility
                    if ($networkProfiles[$index]['orderBy'] == 1) { $display = "table-row-group"; } else { $display = "none"; }
                    
                    // Get Configuration
                    $options = $NetworkProfile->getOptions($networkProfiles[$index]['id']);
                    
                    ?>

                        <tbody id="tbody_<?= $networkProfiles[$index]['id'] ?>" class="options" style="display: <?= $display ?>;">

                            <?php

                                // Display Options
                                for ($index1 = 0; $index1 < count($options); $index1++) 
                                {
                                    if ( $options[$index1]['value'] == 'NULL' ) { $value = 'Not Set'; } else { $value = $options[$index1]['value']; }
                                    
                                    if ($options[$index1]['display'])
                                    {
                                        ?>
                                            <tr>
                                                <td class="key"><?= $options[$index1]['name'] ?></td>
                                                <td class="value"><?= $value ?></td>
                                            </tr>        
                                        <?php
                                    }
                                }

                            ?>

                        </tbody>

                    <?php
                }
                ?>
        </table>
    </div>
</div>


<?php include_once('includes/footer.php'); ?>