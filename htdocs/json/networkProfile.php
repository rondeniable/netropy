<?php

/*
 * This is a simple service to respond to JSON requests
 */
include_once('includes/appHeader.php');
include_once('base/Response.php');
include_once('classes/NetworkProfile.php');

/*
 * Retrieve Variables
 */
$method = $HTTPRequest->get('method');
$networkProfileID = $HTTPRequest->get('networkProfileID');
$engineID = $HTTPRequest->get('engineID');
$pathID = $HTTPRequest->get('pathID');

/*
 * This method returns the search results
 */
function applyNetworkProfile($networkProfileID, $engineID, $pathID)
{
    /*
     * Start Response API
     */
    $Response = new Response();

    try
    {
        $NetworkProfile = new NetworkProfile();
        $NetworkProfile->applyNetworkProfile($networkProfileID, $engineID, $pathID);
    }
    catch(Exception $e)
    {
        $Response->setVariable("code", '500');
        $Response->setVariable("message", 'Failed to Apply Network Profile');
        $Response->setVariable('response',$e->getMessage());
        $Response->respondJSON();
    }

     /*
     * Lets Return the HTML Response
     */
    $Response->setVariable("code", '200');
    $Response->setVariable("message", 'Completed Successfully');
    $Response->respondJSON();

}

/*
 * Method Search
 */
$availableMethods = array('applyNetworkProfile');
switch ($method)
{
    case 'applyNetworkProfile':
        applyNetworkProfile($networkProfileID, $engineID, $pathID);
        break;

    default:
        $Response = new Response();
        $Response->setVariable("code", '500');
        $Response->setVariable("message", 'Unknown Method [' . $method . "]");
        $Response->setVariable('response', 'Available Methods [' . print_r($availableMethods, true) . ']');
        $Response->respondRawJSON();
        break;
}
