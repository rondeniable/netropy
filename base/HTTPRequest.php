<?php
/**
 *      @author Brian Simpson
 *      @date November 14, 2012
 */

class HTTPRequest
{
        private $requestMethod                  = null;
        private $requiredRequestMethod  = null;
        private $formData                               = array();
        private $logger = "";
        /**
         *      Constructor
         */
        function __construct( $requiredRequestMethod = null )
        {
            
                $this->logger = &Log::singleton('file', LOGFILE, TYPE);
    
                $this->requiredRequestMethod = $requiredRequestMethod;


                //-- set the request method.  GET, POST, etc.
                if(array_key_exists('REQUEST_METHOD', $_SERVER))
                {
                        $this->requestMethod = $_SERVER['REQUEST_METHOD'];
                }

                if ($this->requestMethod == 'POST' && array_key_exists('formData',$_POST))
                {
                        $this->formData = array();
                        $tmpArray = explode('&',$_POST['formData']);
                        foreach($tmpArray as $data)
                        {
                                list($name,$value) = explode('=',$data);
                                $this->formData[urldecode($name)] = urldecode($value);
                        }
                }

                if(array_key_exists('SCRIPT_URL', $_SERVER))
                {
                        $this->logger->rawInput('SCRIPT_URL', $_SERVER['SCRIPT_URL']);
                }

        }

        public function get( $fieldName )
        {

                if($this->requiredRequestMethod <> ''  && $this->requestMethod <> $this->requiredRequestMethod )
                {
                        $this->logger->warn('returning no results because requiredRequestMethod is set to['. $this->requiredRequestMethod .'] and requestMethod of ['. $this->requestMethod .'] is being used');
                        return false;
                }


                if( $this->requestMethod == 'GET')
                {
                        if (array_key_exists($fieldName, $_GET))
                        {
                                return $_GET[$fieldName];
                        }
                        else
                        {
                                return null;
                        }
                }
                elseif ($this->requestMethod == 'POST')
                {
                        if (array_key_exists($fieldName, $this->formData))
                        {
                                return $this->formData[$fieldName];
                        }
                        elseif (array_key_exists($fieldName, $_POST))
                        {
                            return $_POST[$fieldName];
                        }
                        elseif (array_key_exists($fieldName, $_GET))
                        {
                            return $_GET[$fieldName];
                        }
                        else
                        {
                                return null;
                        }
                }

                $this->logger->error('unknown REQUEST_METHOD of ['. $this->requestMethod .']');
                return null;

        }

}