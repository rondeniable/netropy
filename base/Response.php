<?php

/*
 * This is a class that hold the JSON responses
 */

class Response
{

    private $code = '';
    private $message = '';
    private $response = '';
    private $callback = '';

    /*
     * Function to set any variable
     */

    public function setVariable($setter, $value)
    {
        if (isset($this->$setter))
        {
            $this->$setter = $value;
        }
        else
        {
            throw new Exception("ERROR: Invalid Setter: [" . $setter . "]");
        }
    }


    /*
     * This method formats, encodes and echos the API Response
     */

    public function respondJSON()
    {
        header('Content-Type: application/json');
        $payload = array('message' => $this->message, 'response' => $this->response);
        echo json_encode($payload);
        exit();
    }

    /*
     * This method is used for tools that require raw JSON for formatting.
     */
    public function respondRawJSON()
    {
        header('Content-Type: application/json');
        echo json_encode($this->response);
        exit();
    }

}
